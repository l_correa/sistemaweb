<?php

class BaseCellDecimal extends BaseCell
{
	private $f;
	
	public function __construct($rawInfo)
	{
		parent::__construct($rawInfo);
		
		$this->f = floatval($rawInfo);
	}
	
	public function compareTo($that)
	{
		if($this->f == $that->f)
		{
			return 0;
		}
		
		return ($this->f < $that->f) ? -1 : 1;
	}
}

?>
