<?php

include_once 'ColumnAlreadyExists.php';
include_once 'ColumnNotFound.php';
include_once 'RowLengthMismatch.php';
include_once 'TypeMismatch.php';
include_once 'RowSorting.php';

class TableData
{
    private $content;
    private $types;
    private $column_order;
    
    private $rawHeader;
    
    private $info_fields;
    
    public function __construct()
    {
        $this->content = array();
        $this->types = array();
        $this->column_order = array();
        
        $this->info_fields = array();
        
        $this->rawHeader = "";
    }
    
    public function addColumn($column_name, $column_type)
    {
        if(array_key_exists($column_name,$this->types))
        {
            throw new ColumnAlreadyExists($column_name);
        }
        
        $this->types[$column_name] = $column_type;
        $this->column_order[] = $column_name;
    }
    
    public function getColumns()
    {
        return $this->column_order;
    }
    
    public function getTypes()
    {
        return $this->types;
    }
    
    public function addRow($row)
    {
        if(count($row) != count($this->column_order))
        {
            throw new RowLengthMismatch(count($row),count($this->column_order));
        }
        
        foreach($row as $pos => $value)
        {
            $t = $this->types[$this->column_order[$pos]];
            
            if(!is_a($value,$t))
            {
                throw new TypeMismatch(get_class($value),$t);
            }
        }
        
        $this->content[] = $row;
    }
    
    public function countRows()
    {
        return count($this->content);
    }
    
    public function getRow($pos)
    {
        if($pos<0)
        {
            $pos = $this->countRows()+$pos;
        }
        
        return $this->content[$pos];
    }
    
    public function getRows($a=0, $b=null)
    {
        if(is_null($b))
        {
            $b=$this->countRows();
        }
        
        if($a<0)
        {
            $a = $this->countRows()+$a;
        }
        
        if($b<0)
        {
            $b = $this->countRows()+$b;
        }
        
        $ret = array();
        
        if($a<$b)
        {
            for(;$a<$b;$a++)
            {
                $ret[] = $this->getRow($a);
            }
        }
        else
        {
            for(;$a>$b;$a--)
            {
                $ret[] = $this->getRow($a-1);
            }
        }
        
        return $ret;
    }
    
    public function getRowAsHashMap($pos)
    {
        if($pos<0)
        {
            $pos = $this->countRows()+$pos;
        }
        
        $map = array();
        $row = $this->getRow($pos);
        
        foreach($this->column_order as $p => $value)
        {
            $map[$value] = $row[$p];
        }
        
        return $map;
    }
    
    public function getRowsAsHashMap($a=0, $b=null)
    {
        if(is_null($b))
        {
            $b=$this->countRows();
        }
        
        if($a<0)
        {
            $a = $this->countRows()+$a;
        }
        
        if($b<0)
        {
            $b = $this->countRows()+$b;
        }
        
        $ret = array();
        
        if($a<$b)
        {
            for(;$a<$b;$a++)
            {
                $ret[] = $this->getRowAsHashMap($a);
            }
        }
        else
        {
            for(;$a>$b;$a--)
            {
                $ret[] = $this->getRowAsHashMap($a-1);
            }
        }
        
        return $ret;
    }
    
    public function setRawHeader($header)
    {
        $this->rawHeader = $header;
    }
    
    public function getRawHeader()
    {
        return $this->rawHeader;
    }
    
    public function setInfoField($field, $value)
    {
        $this->info_fields[$field] = $value;
        
        return $value;
    }
    
    public function getAllInfoFields()
    {
        return $this->info_fields;
    }
    
    public function getAllInfoFieldKeys()
    {
        $fields = array();
        
        foreach($this->info_fields as $key => $value)
        {
            $fields[] = $key;
        }
        
        return $fields;
    }
    
    public function getInfoField($field, $default_value=null)
    {
        if(array_key_exists($field,$this->info_fields))
        {
            return $this->info_fields[$field];
        }
        else
        {
            return $default_value;
        }
    }
    
    public function local_sort($field, $reversed=false)
    {
        if(!array_key_exists($field,$this->types))
        {
            throw new ColumnNotFound($field);
        }
        
        $index = array_search($field,$this->column_order);
        
        $list_to_sort = array();
        
        foreach($this->content as $row)
        {
            $list_to_sort[] = new RowSorting($row,$index);
        }
        
        usort($list_to_sort,array("RowSorting","__compareTo"));
        
        foreach($list_to_sort as $i => $value)
        {
            $this->content[$i] = $list_to_sort[$i]->row;
        }
        
        if($reversed)
        {
            $this->content = array_reverse($this->content);
        }
        
        return $this;
    }
}

?>
