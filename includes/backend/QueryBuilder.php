<?php

class QueryBuilder {
    private $tableData;
    
    function __construct($table=NULL) {
        $this->tableData = $table;
    }
    
    public function insertTable($id_station) {
			return sprintf("INSERT INTO Tabela (estacao) VALUES (%d);", $id_station);
    }
    
    public function insertStation($criador, $id, $tipo, $nome, $cidade, $lat=NULL, $lng=NULL) {
        $header = $this->tableData->getRawHeader();
        
        return  sprintf("INSERT INTO Estacao (criador, id, tipo, nome, cidade, lat, lng, header) VALUES ('%s', %d, %d, '%s', '%s', %f, %f, '%s');", $criador, $id, $tipo, $nome, $cidade, $lat, $lng, $header);
    }
    
    
    public function insertColumns($table_id) {
        $columnsAndTypes = $this->tableData->getTypes();
        $columns = array_keys($columnsAndTypes);
        $types = array_values($columnsAndTypes);
        $query = "";
        
        for ($i = 0; $i < count($columnsAndTypes); $i++) {
            $query .= sprintf("INSERT INTO Coluna (tabela, id, nome, tipo) VALUES (%d, %d, '%s', '%s');", $table_id, $i, $columns[$i], $types[$i]);
        }
        
        return $query;
    }
    
    public function insertRows($table_id, $criador) {
        $rows = $this->tableData->getRows();
        $query = " ";
        foreach($rows as $r => $value) {
            $query .= sprintf("INSERT INTO LinhaDono (tabela, dono, linha) VALUES (%d, '%s', %d);", $table_id, $criador, $r);
        }

        return $query;	
    }
	
    public function insertCells($table_id, $coluna, $linha, $dado) {
        return sprintf("INSERT INTO Celula (tabela, coluna, linha, raw) VALUES (%d, %d, %d, '%s');", $table_id, $coluna, $linha, $dado);

    }

    public function buildLoginQuery($email) {
        return sprintf("SELECT * FROM Usuario WHERE email='%s';",
            mysql_real_escape_string($email));
    }
    
    public function getStations($fields = ["*"]) {
        return sprintf("SELECT %s FROM Estacao;", implode(",", $fields));
    }

    public function getStation($nome) {
        return sprintf("SELECT * FROM Estacao WHERE nome=%s);", $nome);
    }   
 
    public function insertUser($nome, $email, $senha, $admin) {
	return sprintf("INSERT INTO Usuario (nome, email, senha, is_admin) VALUES ('%s', '%s', '%s', %d);", $nome, $email, $senha, $admin);
    }
    
    public function updatePass($email, $senha){
	return sprintf("UPDATE Usuario SET senha='%s' WHERE email='%s';", $senha, $email);
    }
    
}

?>

