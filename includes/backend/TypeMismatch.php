<?php

/**
 * 
 * Exception thrown when someone tries to add a row with an object of the wrong class.
 *
 */

class TypeMismatch extends Exception
{
	private $received;
	private $expected;
	
	public function __construct($r, $e)
	{
		$this->received = $r;
		$this->expected = $e;
	}
	
	public function __toString()
	{
		return "Expected " . $this->expected . ". Received " . $this->received . ".";
	}
}

?>
