<?php

/**
 * 
 * Exception thrown when someone tries to access an column that does not exist.
 *
 */

class ColumnNotFound extends Exception
{
	private $type;
	
	public function __construct($t)
	{
		$this->type = $t;
	}
	
	public function __toString()
	{
		return "Column " . $this->type . " not found.";
	}
}

?>
