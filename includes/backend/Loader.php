<?php

include_once "GlobalParser.php";
include_once "QueryBuilder.php";
include_once "../../config.php";

class Loader {
    private $parser;
    private $db;
    private $queryBuilder;
    
    public function __construct() {
        $this->parser = new GlobalParser();
        $this->db = new mysqli(constant('DB_HOST'), constant('DB_USER'), constant('DB_PASSWORD'), constant('DB_NAME'));
    }

    public function openFile($path) {
        $contents = NULL;

        try {
            $file = fopen($path, "r");
            $contents = fread($file, filesize($path));
            $contents = mb_convert_encoding($contents, "UTF-8", "ISO-8859-1");
            fclose($file);    
        } catch (Exception $e) {
            echo $e->getMEssage();
        }
        
        return $contents;    
    }
    
    public function parse($path) {
        $tableData = NULL;
        try {
            $tableData = $this->parser->parse($this->openFile($path));
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $tableData;
    }
    
    public function insertTableIntoStation($station_id) {
        
    }

    public function insertColumns($station_id) {
        
    }

    private function runQuery($sql) {
        try {
            $this->db->query($query);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function runQueries($station_id, $tableData) {
        $this->queryBuilder = new QueryBuilder($tableData);

        // insert Table into Station (make sure we don't have duplicates)
        //$this->runQuery($this->QueryBuilder->insertTable($station_id));

        // insert Columns
        //$this->runQuery($this->queryBuilder->insertColumns($station_id));

        // Insert Cells
        //echo $this->queryBuilder->insertCells($station_id)
    }
}

$l = new Loader();
$table = $l->parse("/home/felipeborges/Downloads/EH HS02_07102013_1538_COTA-069.txt");

$l->runQueries(0, $table);


?>
