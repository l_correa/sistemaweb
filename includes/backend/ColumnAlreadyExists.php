<?php

/**
 * 
 * Exception thrown when someone tries to add a column that already exists.
 *
 */

class ColumnAlreadyExists extends Exception
{
	private $type;
	
	public function __construct($t)
	{
		$this->type = $t;
	}
	
	public function __toString()
	{
		return "Column " . $this->type . " already exists.";
	}
}

?>
