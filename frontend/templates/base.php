	<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="/static/css/reset.css" type="text/css"/>
		<link rel="stylesheet" href="/static/css/base.css" type="text/css"/>
		
		<link rel="stylesheet" href="/static/fonts/earthbound/font-face.css" type="text/css"/>
		%css%
		<title>Imanust - %title%</title>
	</head>
	<body>
		<div id="container">
			<div class="header">
				<h1 class="logo"><a href="/imanust/home">Imanust</a></h1>
				<ul class="toplinks">
					<li><a href="#">Login</a></li>
					<li><a href="/imanust/request_access">Novo usuario</a></li>
					<li><a href="/imanust/forgot_password">Nova senha</a></li>
					<li><a href="#">Sobre</a></li>
					<li><a href="#">Contato</a></li>
				</ul>
			</div>
				<div class="main_content">
					%body%
				</div>
			</div>
		</div>
	</body>
</html>

<?php

$default_template_context = [
	"title" => "Base",
	"body" => "",
	"css" => "",
];

?>
