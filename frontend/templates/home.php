<?php

$builder = new QueryBuilder();

global $_CACHE;

//$_CACHE->get_cached_and_die_or_start("Home",3600);

$renderer = new Rendered("frontend/templates/base.php");

ob_start();

?>

<link rel="stylesheet" href="/static/css/home.css" type="text/css"/>

<?php

$css = ob_get_contents();
ob_end_clean();

ob_start();

?>

<div class="home">
	<h1>Estações</h1>
	<p>
		Cada estação está posicionada em algum lugar com uma vista bem bonita.
	</p>

	<div id="station_table">
	<?php $result = $_MYSQL->query($builder->getStations(["id","nome","cidade","tipo"]));

	while($row = mysqli_fetch_array($result)) { ?>
        <div id="station_one">
		    <h1><a href="/imanust/station/<?=$row["id"]?>"><?=$row["nome"]?></a></h1>
		    <img src="/static/img/home.jpg">
		    <div class="desc">
			    <p>Essa é uma estação <?=$row["tipo"] ?> muito maneira, a galera toda gosta muito dela. Fica em <?=$row["cidade"]?></p>
			</div>
        </div>
    <?php } ?>
	</div>
</div>

<?php

$body = ob_get_contents();
ob_end_clean();

echo $renderer->render([
	"title" => "Home",
	"body" => $body,
	"css" => $css,
]);

//$_CACHE->finish();

?>
